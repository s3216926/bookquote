package nl.utwente.di.bookQuote;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

public class TemperatureServlet extends HttpServlet {
    private TemperatureConverter temperatureConverter;


    public void init() throws ServletException {
        temperatureConverter = new TemperatureConverter();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Temperature Converter";

        out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE></HEAD>\n" +
                "<BODY>\n" +
                "<H1>" + title + "</H1>\n" +
                "<FORM>\n" +
                "  Enter temperature in Celsius: <INPUT TYPE=\"text\" NAME=\"celsius\"><BR>\n" +
                "  <INPUT TYPE=\"submit\" VALUE=\"Convert\">\n" +
                "</FORM>\n");

        // Convert temperature from Celsius to Fahrenheit if parameter is present
        String celsiusParam = request.getParameter("celsius");
        if (celsiusParam != null && !celsiusParam.isEmpty()) {
            double celsius = Double.parseDouble(celsiusParam);
            double fahrenheit = temperatureConverter.celsiusToFahrenheit(celsius);
            out.println("<P>Temperature in Fahrenheit: " + fahrenheit + "</P>\n");
        }

        out.println("</BODY></HTML>");
    }
}
