package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private final HashMap<String, Double> bookPrices;

    public Quoter() {
        // Initialize bookPrices HashMap and populate with prices
        bookPrices = new HashMap<>();
        bookPrices.put("1", 10.0);
        bookPrices.put("2", 45.0);
        bookPrices.put("3", 20.0);
        bookPrices.put("4", 35.0);
        bookPrices.put("5", 50.0);
    }

    public double getBookPrice(String isbn) {
        // Retrieve price from HashMap or return 0 if not found
        return bookPrices.getOrDefault(isbn, 0.0);
    }
}
